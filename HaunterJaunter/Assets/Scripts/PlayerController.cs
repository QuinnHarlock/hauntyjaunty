﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Text countText;
    private int count;
    void Start()
    {
        count = 0;
        SetCountText();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("KeyPickup"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Key: " + count.ToString();
    }

        // Update is called once per frame
    void Update()
    {
        
    }
}
