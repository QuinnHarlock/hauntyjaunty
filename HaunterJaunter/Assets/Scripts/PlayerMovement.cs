﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerMovement : MonoBehaviour
{
    float m_Timer;
    bool m_HasAudioPlayed;
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    bool m_IsPlayerCaught;
    bool m_IsPlayerAtExit;
    public CanvasGroup instructionBackgroundImageCanvasGroup;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public GameObject player;
    bool m_HasGameStarted;

    public float turnSpeed = 20f;
    public Text countText;
    private int count;

    public float force;
    private Rigidbody rb;
    float myTime = 60;
    public Text timer;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_movement;
    Quaternion m_Rotation = Quaternion.identity;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        
        count = 0;
        SetCountText();
        countText.text = "";
        m_HasGameStarted = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            m_HasGameStarted = true;
            GameObject.FindGameObjectWithTag("Instruction").SetActive(false);
        }
        if (m_HasGameStarted == true)
        {
            timer.text = "Time left: " + myTime.ToString("f0");
            myTime -= Time.deltaTime;
        }
        

        if (Input.GetKeyDown("space") && GetComponent<Rigidbody>().transform.position.y <= 0.2f)
        {
            Vector3 jump = new Vector3(0.0f, 200.0f, 0.0f);

            GetComponent<Rigidbody>().AddForce(jump);
        }

        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
            GameObject.FindGameObjectWithTag("Timer").SetActive(false);

        }

        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
            GameObject.FindGameObjectWithTag("Timer").SetActive(false);
        }        
        
        else if (myTime < 0)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
            GameObject.FindGameObjectWithTag("Timer").SetActive(false);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_movement.Set(horizontal, 0f, vertical);
        m_movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("KeyPickup"))
        {
            other.gameObject.SetActive(false);
            GameObject.FindGameObjectWithTag("EndBlock").SetActive(false);
            count = count + 1;
            SetCountText();
        }
        if (other.gameObject.CompareTag("End"))
        {
            m_IsPlayerAtExit = true;
        }
    }
    void SetCountText()
    {
        if (count == 1)
        {
            countText.text = "Key Got!";
        }
        
    }
    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }

            else
            {
                Application.Quit();
            }
        }
    }
}
